package com.member.bo;

public class BenefitInfo {
	private int planId;
	private String planName;
	private float deductible;
	private int coInsurance;
	private float coinsuranceMax;
	private float outofpocketMaximum;
	/**
	 * @return the plan_id
	 */
	
	public BenefitInfo() {
		// TODO Auto-generated constructor stub
	}
	public BenefitInfo(int planId, String planName, float deductible, int coInsurance, float coinsuranceMax,
			float outofpocketMaximum) {
		super();
		this.planId = planId;
		this.planName = planName;
		this.deductible = deductible;
		this.coInsurance = coInsurance;
		this.coinsuranceMax = coinsuranceMax;
		this.outofpocketMaximum = outofpocketMaximum;
	}
	/**
	 * @return the planId
	 */
	public int getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	/**
	 * @return the deductible
	 */
	public float getDeductible() {
		return deductible;
	}
	/**
	 * @param deductible the deductible to set
	 */
	public void setDeductible(float deductible) {
		this.deductible = deductible;
	}
	/**
	 * @return the coInsurance
	 */
	public int getCoInsurance() {
		return coInsurance;
	}
	/**
	 * @param coInsurance the coInsurance to set
	 */
	public void setCoInsurance(int coInsurance) {
		this.coInsurance = coInsurance;
	}
	/**
	 * @return the coinsuranceMax
	 */
	public float getCoinsuranceMax() {
		return coinsuranceMax;
	}
	/**
	 * @param coinsuranceMax the coinsuranceMax to set
	 */
	public void setCoinsuranceMax(float coinsuranceMax) {
		this.coinsuranceMax = coinsuranceMax;
	}
	/**
	 * @return the outofpocketMaximum
	 */
	public float getOutofpocketMaximum() {
		return outofpocketMaximum;
	}
	/**
	 * @param outofpocketMaximum the outofpocketMaximum to set
	 */
	public void setOutofpocketMaximum(float outofpocketMaximum) {
		this.outofpocketMaximum = outofpocketMaximum;
	}
	
}
