package com.member.bo;
import java.sql.Date;
public class ClaimInfo {
	private String memberId;
	private String claimId;
	private String status;
	private String physician;
	private Date servicedate;
	public ClaimInfo(String memberId,String claimId, Date servicedate, String physician, String status) {
		super();
		this.setMemberId(memberId);
		this.claimId = claimId;
		this.servicedate = servicedate;
		this.physician = physician;
		this.status = status;
	}
	public ClaimInfo() {
		// TODO Auto-generated constructor stub
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPhysician() {
		return physician;
	}
	public void setPhysician(String physician) {
		this.physician = physician;
	}
	public Date getServicedate() {
		return servicedate;
	}
	public void setServicedate(Date servicedate) {
		this.servicedate = servicedate;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
}
