package com.member.bo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PhysicianInfo {
	private Integer physicianId;
	private String physicianName;
	private String specilist;
	private String address;
	private String healthcareFelicityName;
	private Integer zipcode;
	private Integer contactNo;
	private String activeStaus;
	public PhysicianInfo(Integer physicianId, String physicianName, String specilist, String address, String healthcareFelicityName,
			Integer zipcode, Integer contactNo,String activeStaus) {
		super();
		this.physicianId = physicianId;
		this.physicianName = physicianName;
		this.specilist = specilist;
		this.address = address;
		this.healthcareFelicityName = healthcareFelicityName;
		this.zipcode = zipcode;
		this.contactNo = contactNo;
		this.activeStaus = activeStaus;
	}

	public PhysicianInfo() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the physicianId
	 */
	public Integer getPhysicianId() {
		return physicianId;
	}
	
	/**
	 * @param physicianId the physicianId to set
	 */
	public void setPhysicianId(Integer physicianId) {
		this.physicianId = physicianId;
	}

	/**
	 * @return the physicianName
	 */
	public String getPhysicianName() {
		return physicianName;
	}


	/**
	 * @param physicianName the physicianName to set
	 */
	public void setPhysicianName(String physicianName) {
		this.physicianName = physicianName;
	}


	/**
	 * @return the specilist
	 */
	public String getSpecilist() {
		return specilist;
	}


	/**
	 * @param specilist the specilist to set
	 */
	public void setSpecilist(String specilist) {
		this.specilist = specilist;
	}


	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}


	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}


	/**
	 * @return the healthcareFelicityName
	 */
	public String getHealthcareFelicityName() {
		return healthcareFelicityName;
	}


	/**
	 * @param healthcareFelicityName the healthcareFelicityName to set
	 */
	public void setHealthcareFelicityName(String healthcareFelicityName) {
		this.healthcareFelicityName = healthcareFelicityName;
	}


	/**
	 * @return the zipcode
	 */
	public Integer getZipcode() {
		return zipcode;
	}


	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(Integer zipcode) {
		this.zipcode = zipcode;
	}


	/**
	 * @return the contactNo
	 */
	public Integer getContactNo() {
		return contactNo;
	}


	/**
	 * @param contactNo the contactNo to set
	 */
	public void setContactNo(Integer contactNo) {
		this.contactNo = contactNo;
	}

	public String getActiveStaus() {
		return activeStaus;
	}

	public void setActiveStaus(String activeStaus) {
		this.activeStaus = activeStaus;
	}
	
}
