package com.member.bo;

public class InsuranceInfo {
	private String state;
	private String InsuraneName;
	private Integer zipCode;

	public InsuranceInfo() {
		// TODO Auto-generated constructor stub
	}
	public InsuranceInfo(String state, String insuraneName, Integer zipCode) {
		super();
		this.state = state;
		InsuraneName = insuraneName;
		this.zipCode = zipCode;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the insuraneName
	 */
	public String getInsuraneName() {
		return InsuraneName;
	}

	/**
	 * @param insuraneName the insuraneName to set
	 */
	public void setInsuraneName(String insuraneName) {
		InsuraneName = insuraneName;
	}

	/**
	 * @return the zipCode
	 */
	public Integer getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

}
