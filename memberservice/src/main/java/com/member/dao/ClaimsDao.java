package com.member.dao;

import java.sql.Connection;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.member.bo.ClaimInfo;

public class ClaimsDao {
	// JDBC driver name and database URL
	//static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	//static final String DB_URL = "jdbc:mysql://localhost/member";
	// static final Logger log = LogManager.getLogger(DataAccess.class);
	// Database credentials
	//static final String USER = "root";
	//static final String PASS = "password";

	public Boolean addClaimInfo(ClaimInfo claimInfo) {
		// log.info("Entring");
		//Connection conn = null;
		boolean responseStatus = false;
		Context initContext;
		Context envContext;
		DataSource ds = null;
		try {
			initContext = new InitialContext();
			envContext  = (Context)initContext.lookup("java:/comp/env");
			ds = (DataSource)envContext.lookup("jdbc/memberdb");
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try (Connection conn = ds.getConnection();){

			// Register JDBC driver
			//Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			//conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			
			PreparedStatement pstmt = conn.prepareStatement(
					"insert into member.claim_info (member_id,claim_id,service_date,physician,status) values(?,?,?,?,?)");
			// ResultSet rs = pstmt.executeQuery();
			// For the first parameter,
			// get the data using request object
			// sets the data to stmt pointer
			pstmt.setInt(1, Integer.valueOf(claimInfo.getMemberId()));
			pstmt.setInt(2, Integer.valueOf(claimInfo.getClaimId()));
			pstmt.setDate(3, (new java.sql.Date(claimInfo.getServicedate().getTime())));
			pstmt.setString(4, claimInfo.getPhysician());
			pstmt.setString(5, claimInfo.getStatus());
			// Execute the insert command using executeUpdate()
			// to make changes in database
			if (pstmt.executeUpdate() != 0) {
				responseStatus = true;
			}
			pstmt.close();
			// Clean-up environment
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			// log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			// log.error("Errors Exception", e);
		} /*finally {
			// finally block used to close resources
			try {

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				// log.error("SQLException", se2);
			} // nothing we can do
				// log.debug("Exiting");
		}*/
		return responseStatus;
	}

	public List<ClaimInfo> getClaimInfo(Integer memberId) {
		// log.info("Entring");
		List<ClaimInfo> claimInfoList = new ArrayList();

		// String login = null;
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			//Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			//conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("jdbc/memberdb");
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql;
			// log.info("Fetching the ClaimInformation of " + memberId);
			sql = "SELECT * FROM claim_info WHERE member_id = " + memberId;
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				// log.info("Retrieve by column name");
				ClaimInfo claimInfo = new ClaimInfo();
				claimInfo.setMemberId(rs.getString("member_id"));
				claimInfo.setClaimId(rs.getString("claim_id"));
				claimInfo.setPhysician(rs.getString("physician"));
				claimInfo.setServicedate(rs.getDate("service_date"));
				claimInfo.setStatus(rs.getString("status"));
				claimInfoList.add(claimInfo);
			}

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			// log.error("Errors in SQLException", se);
		} catch (Exception e) {
			System.out.println(e);
			// Handle errors for Class.forName
			// log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				// log.error("SQLException", se2);
			} // nothing we can do
				// log.debug("Exiting");
			return claimInfoList;
		}
	}
}
