package com.member.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.member.bo.InsuranceInfo;

public class InsuranceDao {
	// JDBC driver name and database URL
	//static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	//static final String DB_URL = "jdbc:mysql://localhost/member";
	// static final Logger log = LogManager.getLogger(DataAccess.class);
	// Database credentials
	//static final String USER = "root";
	//static final String PASS = "password";

	@SuppressWarnings("finally")
	public List<InsuranceInfo> getInsuranceInfo(int zipcode) {
		// log.info("Entring");
		List<InsuranceInfo> insuranceInfoList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			//Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			//conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("jdbc/memberdb");
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql;
			// log.info("Fetching the PhysicianInformation of " + zipcode);
			sql = "SELECT * FROM member.insurance_info WHERE zip_code=" + zipcode;
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				// log.info("Retrieve by column name");
				InsuranceInfo insuranceInfo = new InsuranceInfo();
				insuranceInfo.setZipCode(rs.getInt("zip_code"));
				insuranceInfo.setState(rs.getString("state"));
				insuranceInfo.setInsuraneName(rs.getString("insurance_name"));
				insuranceInfoList.add(insuranceInfo);
			}

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			// log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			// log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				// log.error("SQLException", se2);
			} // nothing we can do
				// log.debug("Exiting");
			return insuranceInfoList;
		}
	}

}
