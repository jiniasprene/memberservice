package com.member.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.member.bo.PhysicianInfo;

public class PhysicianDao {
	
	// JDBC driver name and database URL
		//static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		//static final String DB_URL = "jdbc:mysql://localhost/member";
		//static final Logger log = LogManager.getLogger(DataAccess.class);
		// Database credentials
		//static final String USER = "root";
		//static final String PASS = "password";
		
	@SuppressWarnings("finally")
	public List<PhysicianInfo> getPhysicianInfo(Integer zipcode) {
		//log.info("Entring");
		List<PhysicianInfo> physicianInfoList = new ArrayList();
		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			//Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			//conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("jdbc/memberdb");
			conn = ds.getConnection();
			stmt = conn.createStatement();
			String sql;
			//log.info("Fetching the PhysicianInformation of " + zipcode);
			sql = "SELECT * FROM member.physician_info WHERE zipcode=" + zipcode;
			ResultSet rs = stmt.executeQuery(sql);
			
			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				//log.info("Retrieve by column name");
				PhysicianInfo physicianInfo = new PhysicianInfo();
				physicianInfo.setPhysicianId(rs.getInt("physician_id"));
				physicianInfo.setPhysicianName(rs.getString("physician_name"));
				physicianInfo.setSpecilist(rs.getString("specilist"));
				physicianInfo.setAddress(rs.getString("address"));
				physicianInfo.setZipcode(rs.getInt("zipcode"));
				physicianInfo.setHealthcareFelicityName(rs.getString("healthcare_felicityname"));
				physicianInfo.setContactNo(rs.getInt("contact_no"));
				physicianInfo.setActiveStaus(rs.getString("active_status"));
				physicianInfoList.add(physicianInfo);
			}

			// Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			//log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			//log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				//log.error("SQLException", se2);
			} // nothing we can do
			//log.debug("Exiting");
			return physicianInfoList;
		}
	}
	@SuppressWarnings("finally")
	public boolean addPhysicianInfo(PhysicianInfo physicianInfo) {
		boolean status = false;
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			// Register JDBC driver
			//Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			//conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("jdbc/memberdb");
			conn = ds.getConnection();
			
			String sql = "INSERT INTO member.physician_info (physician_id, physician_name, specilist, address, zipcode, healthcare_felicityname, contact_no) VALUES (?,?,?,?,?,?,?)";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, physicianInfo.getPhysicianId());
			ps.setString(2, physicianInfo.getPhysicianName());
			ps.setString(3, physicianInfo.getSpecilist());
			ps.setString(4, physicianInfo.getAddress());
			ps.setInt(5, physicianInfo.getZipcode());
			ps.setString(6, physicianInfo.getHealthcareFelicityName());
			ps.setInt(7, physicianInfo.getContactNo());
			int rs = ps.executeUpdate();
			
			// Extract data from result set
			if (rs == 1) {
				status = true;
			}
			// Clean-up environment
			ps.close();
			conn.close();
		} catch (SQLException se) {
			System.out.println("SQLException: " + se.getMessage());
			// Handle errors for JDBC
			//log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			//log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (ps != null) {
					ps.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				//log.error("SQLException", se2);
			} // nothing we can do
			//log.debug("Exiting");
			return status;
		}
	}
	@SuppressWarnings("finally")
	public boolean deletePhysicianInfo(int physicianId) {
		boolean status = false;
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			// Register JDBC driver
			//Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			//conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("jdbc/memberdb");
			conn = ds.getConnection();
			
			String sql = "DELETE FROM  member.physician_info WHERE physician_id=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, physicianId);
			int rs = ps.executeUpdate();
			
			// Extract data from result set
			if (rs == 1) {
				status = true;
			}
			// Clean-up environment
			ps.close();
			conn.close();
		} catch (SQLException se) {
			System.out.println("SQLException: " + se.getMessage());
			// Handle errors for JDBC
			//log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			//log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (ps != null) {
					ps.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				//log.error("SQLException", se2);
			} // nothing we can do
			//log.debug("Exiting");
			return status;
		}
	}
	@SuppressWarnings("finally")
	public boolean updatePhysicianInfo(PhysicianInfo physicianInfo) {
		boolean status = false;
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			// Register JDBC driver
			//Class.forName("com.mysql.jdbc.Driver");
			// Open a connection
			//conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Execute SQL query
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("jdbc/memberdb");
			conn = ds.getConnection();
			
			String sql = "UPDATE member.physician_info SET physician_name=?, specilist=?, address=?, zipcode=?, healthcare_felicityname=?,contact_no=? WHERE (physician_id=?)";
			ps = conn.prepareStatement(sql);
			ps.setInt(7, physicianInfo.getPhysicianId());
			ps.setString(1, physicianInfo.getPhysicianName());
			ps.setString(2, physicianInfo.getSpecilist());
			ps.setString(3, physicianInfo.getAddress());
			ps.setInt(4, physicianInfo.getZipcode());
			ps.setString(5, physicianInfo.getHealthcareFelicityName());
			ps.setInt(6, physicianInfo.getContactNo());
			int rs = ps.executeUpdate();
					
			// Extract data from result set
			if (rs == 1) {
				status = true;
			}
			// Clean-up environment
			ps.close();
			conn.close();
		} catch (SQLException se) {
			System.out.println("SQLException: " + se.getMessage());
			// Handle errors for JDBC
			//log.error("Errors in SQLException", se);
		} catch (Exception e) {
			// Handle errors for Class.forName
			//log.error("Errors Exception", e);
		} finally {
			// finally block used to close resources
			try {
				if (ps != null) {
					ps.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se2) {
				//log.error("SQLException", se2);
			} // nothing we can do
			//log.debug("Exiting");
			return status;
		}
	}
	
}
