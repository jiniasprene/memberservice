package com.member.service.claimservice;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.member.bo.ClaimInfo;
import com.member.dao.ClaimsDao;

@Path("/claimservice")

public class ClaimService {
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public boolean inserClaimsInfo(ClaimInfo claimInfo) {
		ClaimsDao claimsDao = new ClaimsDao();
		boolean status = claimsDao.addClaimInfo(claimInfo);
		return status;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{memberid}")
	public List<ClaimInfo> getClaimInfo(@PathParam("memberid") int memberId){
		ClaimsDao claimsDao = new ClaimsDao();
		List<ClaimInfo> claimInfoList = claimsDao.getClaimInfo(memberId);
		return claimInfoList;
	}
}
