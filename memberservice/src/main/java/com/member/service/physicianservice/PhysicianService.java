
package com.member.service.physicianservice;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.member.bo.PhysicianInfo;
import com.member.dao.PhysicianDao;

/** Example resource class hosted at the URI path "/myresource"
 */
@Path("/physicianservice")
public class PhysicianService {
    
    @GET 
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<PhysicianInfo> getPhysicianInfo(@QueryParam("zip") int zipcode) {
    	//System.out.println("zip" +zipcode);
    	PhysicianDao physicianDao = new PhysicianDao();
    	List<PhysicianInfo> physicianInfoList = physicianDao.getPhysicianInfo(zipcode);
    	return physicianInfoList;
    }
    
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Boolean putPhysicianInfo(PhysicianInfo physicianInfo) {
    	PhysicianDao physicianDao = new PhysicianDao();
    	boolean physicianInfoOutput = physicianDao.addPhysicianInfo(physicianInfo);
		return physicianInfoOutput;
    }
    
    @DELETE
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Boolean deletePhysicianInfo(@QueryParam("pid") int physicianId) {
    	PhysicianDao physicianDao = new PhysicianDao();
    	boolean physicianInfoOutput = physicianDao.deletePhysicianInfo(physicianId);
		return physicianInfoOutput;
    }
   
    @PUT
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Boolean updatePhysicianInfo(PhysicianInfo physicianInfo) {
    	PhysicianDao physicianDao = new PhysicianDao();
    	boolean physicianInfoOutput = physicianDao.updatePhysicianInfo(physicianInfo);
		return physicianInfoOutput;
    }
    /*@GET 
    @Produces(MediaType.APPLICATION_XML)
    public List<MemberInfo> getMemberInfo() {
    	List<MemberInfo> memberInfoLst = new ArrayList();
    	MemberInfo memberInfo = new MemberInfo();
    	memberInfo.setMemberId(1234);
    	memberInfo.setFirstName("Oreo");
    	memberInfoLst.add(memberInfo);
    	MemberInfo memberInfo1 = new MemberInfo();
    	memberInfo1.setMemberId(5678);
    	memberInfo1.setFirstName("Test");
    	memberInfoLst.add(memberInfo1);
        return memberInfoLst;
    }*/
    
   /* @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public PhysicianInfo putPhysicianInfo(PhysicianInfo physicianInfo) {
    	System.out.println(physicianInfo.getPhysicianName());
		return physicianInfo;
    }*/
    
}
