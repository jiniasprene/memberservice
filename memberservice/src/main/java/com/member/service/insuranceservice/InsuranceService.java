package com.member.service.insuranceservice;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.member.bo.InsuranceInfo;
import com.member.dao.InsuranceDao;

@Path("/insuranceservice")
public class InsuranceService {

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<InsuranceInfo> getInsuranceInfo(@QueryParam("zip") int zipcode) {
		InsuranceDao insuranceDao = new InsuranceDao();
		List<InsuranceInfo> insuranceInfoList = insuranceDao.getInsuranceInfo(zipcode);
		return insuranceInfoList;
	}
}
